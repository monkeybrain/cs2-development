const express = require("express");
const router = express.Router();
const User = require("../models/User")
const bcrypt = require("bcrypt");
const userController = require("../controllers/user");

//User Registration
router.post("/register", async (req, res) => {
	const newUser = new User({
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10)
	});

	try {
	const savedUser = await newUser.save();
	res.status(200).json(savedUser)
	} catch (err) {
	res.status(500).json(err)
	}
});

//Login

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;