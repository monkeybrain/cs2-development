const mongoose = require('mongoose')

const OrderSchema = new mongoose.Schema({

	userID: {
		type: String,
		required: [true, 'User ID is required']
	},

	products: [
	{
		productId: {
			type: String,
		},
		quantity: {
			type: Number,
		}

	}],

	totalAmount: {
		type: Number,
		required: true
	},

	status: {
		type: String,
		default: "pending"
	},

	purchasedOn: {
	type: Date,
	default: new Date()
		}
});

module.exports = mongoose.model('Order', OrderSchema)