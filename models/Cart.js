const mongoose = require('mongoose')

const CartSchema = new mongoose.Schema({

	userID: {
		type: String,
		required: [true, 'User ID is required']
	},

	products: [
	{
		productId: {
			type: String,
		},
		quantity: {
			type: Number,
		},
		createdOn: {
		type: Date,
		default: new Date()
		}

	}]
});

module.exports = mongoose.model('Cart', CartSchema)