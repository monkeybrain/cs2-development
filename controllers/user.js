const User = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const secret = "CS2 Development";

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)} 
			} else {
					return false
				}
			}
	})
};

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if (typeof token !== "undefined") {
		console.log(token)

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return res.send({auth: "failed"})
			} else {
				next()
			}
		})
	} else {
		return res.send({auth: "failed"})
	}
}

module.exports.decode = (token) => {

	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if (err) {
				return null
			} else {
				return jwt.decode(token, {complete:true}).payload
			}
		})
	} else {
		return null
	}
}